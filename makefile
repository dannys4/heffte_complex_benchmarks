CC		= g++
LD		= g++
HEFFTEPATH  = /home/dannys4/git-repos/heffte_icl/buildAVX512/include
CFLAGS  = -Wall -I$(HEFFTEPATH) -llibheffte -mavx -mfma -mavx512f -O3
LDFLAGS =

SOURCES = benchComplex.cpp
OBJECTS = $(SOURCES:.cpp=.o)

TARGET  = benchmark

.PHONY: all
all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(LD) $(LDFLAGS) $^ -o $@

%.o: %.cpp
	$(CC) $(CFLAGS) $< -c -o $@

clean:
	rm $(TARGET) $(OBJECTS)