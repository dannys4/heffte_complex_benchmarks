#include<iostream>
#include<array>
#include<chrono>
#include<random>
#include<algorithm>
#include<functional>
#include<tuple>
#include"heffte.h"

enum BenchOp {addOp, subOp, mulOp, divOp};
using std::chrono::duration_cast;
using std::chrono::nanoseconds;

constexpr size_t num_samples = 100;


template<typename F, int L, BenchOp Op>
std::pair<nanoseconds, nanoseconds> benchComplex(size_t N) {
    using clock = std::chrono::high_resolution_clock;
    auto rand_cpx = std::bind(std::uniform_real_distribution<>{-5,5}, std::default_random_engine{});

    heffte::stock::complex_vector<F,L> stock_arr_l (N);
    heffte::stock::complex_vector<F,L> stock_arr_r (N);
    std::vector<std::array<std::complex<F>,L/2>> comp_arr_l (N);
    std::vector<std::array<std::complex<F>,L/2>> comp_arr_r (N);

    heffte::stock::complex_vector<F,L> stock_arr_out (N);
    std::vector<std::array<std::complex<F>,L/2>> comp_arr_out (N);

    for(size_t i = 0; i < N; i++) {
        std::array<std::complex<F>,L/2> tmp_arr_l {};
        std::array<std::complex<F>,L/2> tmp_arr_r {};
        for(int j = 0; j < L/2; j++) {
            F real_r = rand_cpx(); F comp_r = rand_cpx();
            F real_l = rand_cpx(); F comp_l = rand_cpx();
            tmp_arr_r[j] = std::complex<F>(real_r, comp_r);
            tmp_arr_l[j] = std::complex<F>(real_l, comp_l);
        }
        stock_arr_l.push_back(heffte::stock::Complex<F,L> {tmp_arr_l.data()});
        stock_arr_r.push_back(heffte::stock::Complex<F,L> {tmp_arr_r.data()});
        stock_arr_out.push_back(heffte::stock::Complex<F,L> {});
        comp_arr_l.push_back(tmp_arr_l);
        comp_arr_r.push_back(tmp_arr_r);
        comp_arr_out.push_back(std::array<std::complex<F>,L/2> {});
    }
    auto start = clock::now();
    for(size_t i = 0; i < N; i++) {
        switch(Op) {
            case addOp: stock_arr_out[i] = stock_arr_l[i] + stock_arr_r[i]; break;
            case subOp: stock_arr_out[i] = stock_arr_l[i] - stock_arr_r[i]; break;
            case mulOp: stock_arr_out[i] = stock_arr_l[i] * stock_arr_r[i]; break;
            case divOp: stock_arr_out[i] = stock_arr_l[i] / stock_arr_r[i]; break;
        }
        // if(rand_cpx() > 5) std::cout << "no!\n";
    }
    auto end = clock::now();
    auto stock_elapsed = duration_cast<nanoseconds>(end - start);
    start = clock::now();
    for(size_t i = 0; i < N; i++) {
        for(int j = 0; j < L/2; j++) {
            switch(Op) {
                case addOp: comp_arr_out[i][j] = comp_arr_l[i][j] + comp_arr_r[i][j]; break;
                case subOp: comp_arr_out[i][j] = comp_arr_l[i][j] - comp_arr_r[i][j]; break;
                case mulOp: comp_arr_out[i][j] = comp_arr_l[i][j] * comp_arr_r[i][j]; break;
                case divOp: comp_arr_out[i][j] = comp_arr_l[i][j] / comp_arr_r[i][j]; break;
            }
        }
        // if(rand_cpx() > 5) std::cout << "no!\n"; 
    }
    end = clock::now();
    auto ref_elapsed = duration_cast<nanoseconds>(end - start);
    return std::pair<nanoseconds, nanoseconds> {stock_elapsed, ref_elapsed};
}

template<typename F, size_t S>
void new_timing(std::array<size_t, S>& Ns, std::array<std::tuple<double, double, double>, S>& timings) {
    using clock = std::chrono::high_resolution_clock;
    constexpr int L = std::is_same<F, float>::value ? 16 : 8;
    constexpr int L2 = L / 2;
    auto rand_cpx = std::bind(std::uniform_real_distribution<>{-5,5}, std::default_random_engine{});

    std::vector<std::complex<F>> ref_in1(L2*Ns[S-1]);
    std::vector<std::complex<F>> ref_in2(L2*Ns[S-1]);
    std::vector<std::complex<F>> ref_out(L2*Ns[S-1]);
    heffte::stock::complex_vector<F, L2> comp1_in1(2*Ns[S-1]);
    heffte::stock::complex_vector<F, L2> comp1_in2(2*Ns[S-1]);
    heffte::stock::complex_vector<F, L2> comp1_out(2*Ns[S-1]);
    heffte::stock::complex_vector<F, L> comp2_in1(Ns[S-1]);
    heffte::stock::complex_vector<F, L> comp2_in2(Ns[S-1]);
    heffte::stock::complex_vector<F, L> comp2_out(Ns[S-1]);
    for(size_t i = 0; i < Ns[S-1]; i++) {
        std::complex<F>* end_in1 = ref_in1.data() + L2*i;
        std::complex<F>* end_in2 = ref_in2.data() + L2*i;
        for(int j = 0; j < L2; j++) {
            ref_in1[i*L2 + j] = std::complex<F>{(F) rand_cpx(), (F) rand_cpx()};
            ref_in2[i*L2 + j] = std::complex<F>{(F) rand_cpx(), (F) rand_cpx()};
        }
        comp1_in1[i*2]     = heffte::stock::Complex<F,L2>(end_in1);
        comp1_in1[i*2 + 1] = heffte::stock::Complex<F,L2>(end_in1 + L2/2);
        comp1_in2[i*2]     = heffte::stock::Complex<F,L2>(end_in2);
        comp1_in2[i*2 + 1] = heffte::stock::Complex<F,L2>(end_in2 + L2/2);
        comp2_in1[i] = heffte::stock::Complex<F,L> (end_in1);
        comp2_in2[i] = heffte::stock::Complex<F,L> (end_in2);
    }
    std::cout << "Starting loop... with S = " << S << "\n";
    for(size_t i = 0; i < S; i++) {
        size_t N = Ns[i];
        std::cout << "N = " << N << ", " << "i = " << i << "\n";
        auto start1 = clock::now();
        for(size_t j = 0; j < N; j++) {
            for(int k = 0; k < L2; k++) ref_out[L2*j + k] = ref_in1[L2*j + k]*ref_in2[L2*j + k];
        }
        auto end1 = clock::now();
        if(rand_cpx() < -5) std::cout << ref_out[0] << "\n";
        auto start2 = clock::now();
        for(size_t j = 0; j < N; j++) {
            comp1_out[2*j] = comp1_in1[2*j]*comp1_in2[2*j];
            comp1_out[2*j+1] = comp1_in1[2*j+1]*comp1_in2[2*j+1];
        }
        auto end2 = clock::now();
        if(rand_cpx() < -5) std::cout << comp1_out[0] << "\n";
        auto start3 = clock::now();
        for(size_t j = 0; j < N; j++) {
            comp2_out[j] = comp2_in1[j] * comp2_in2[j];
        }
        auto end3 = clock::now();
        if(rand_cpx() < -5) std::cout << comp2_out[0] << "\n";
        auto time_std = duration_cast<nanoseconds>(end1 - start1).count() * 1.e-9;
        auto time_avx = duration_cast<nanoseconds>(end2 - start2).count() * 1.e-9;
        auto time_avx512 = duration_cast<nanoseconds>(end3 - start3).count() * 1.e-9;
        timings[i] = std::tuple<double, double, double> {time_std, time_avx, time_avx512};
    }
    std::cout << "Finished loop!\n";
}

int main() {
    constexpr double num_samples_d = (double) num_samples;
    auto rand_factor = std::bind(std::uniform_real_distribution<>{1 + (1/num_samples_d),1 + (30. / num_samples_d)}, std::default_random_engine{});
    int start_N = 20;
    auto new_N = [&rand_factor](int N) {return (int) (rand_factor()*((double) N));};
    std::array<size_t, num_samples> Ns {};
    std::array<std::tuple<double, double, double>, num_samples> timings {};
    Ns[0] = start_N;
    for(size_t i = 1; i < num_samples; i++) Ns[i] = new_N(Ns[i-1]);
    std::cout << "Max N = " << Ns[num_samples-1] << "\n";
    new_timing<float, num_samples>(Ns, timings);
    std::cout << "i,N,std,stock_AVX,stock_AVX512\n";
    for(size_t i = 0; i < num_samples; i++) {
        auto tmp = timings[i];
        std::cout << i << "," << Ns[i] << "," << std::get<0>(tmp) << "," << std::get<1>(tmp) << "," << std::get<2>(tmp) << "\n";
    }
}
